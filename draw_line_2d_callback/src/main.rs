/*
 * This is a simple, self contained example of how you can plot a line on a grid, using the following command:
 *
 * ./draw_line_callback --width=80 --height=60 --x0=4 --y0=10 --x1=70 --y1=44
 *
 * This example is an example of:
 *
 * - Passing a callback to a function.
 * - Simple argument parsing.
 */


use std::str::FromStr;

/* ---------------------------------------------------------------------------
 * Utility Macro's */

macro_rules! elem {
    ($val:expr, $($var:expr), *) => {
        $($val == $var) || *
    }
}


/* ---------------------------------------------------------------------------
 * Plot Line */

fn plot_line_v2v2i<F>(
    p1: [i32; 2],
    p2: [i32; 2],
    callback: &mut F)
    where
    F: FnMut(i32, i32) -> bool
{
    let mut x1: i32 = p1[0];
    let mut y1: i32 = p1[1];
    let x2: i32 = p2[0];
    let y2: i32 = p2[1];

    let x_sign: i32;
    let y_sign: i32;

    let delta_x: i32;
    let delta_y: i32;
    let delta_x_step: i32;
    let delta_y_step: i32;

    // if x1 == x2 or y1 == y2, then it does not matter what we set here
    if x2 > x1 {
        x_sign = 1;
        delta_x = x2 - x1;
    } else {
        x_sign = -1;
        delta_x = x1 - x2;
    }

    if y2 > y1 {
        y_sign = 1;
        delta_y = y2 - y1;
    } else {
        y_sign = -1;
        delta_y = y1 - y2;
    }

    delta_x_step = 2 * delta_x;
    delta_y_step = 2 * delta_y;

    if callback(x1, y1) == false {
        return;
    }

    if delta_x >= delta_y {
        // error may go below zero
        let mut error = delta_y_step - delta_x;

        while x1 != x2 {
            if error >= 0 {
                if error != 0 || (x_sign == 1) {
                    y1 += y_sign;
                    error -= delta_x_step;
                }
                // else do nothing
            }
            // else do nothing

            x1 += x_sign;
            error += delta_y_step;

            if callback(x1, y1) == false {
                // signal that caller wants to exit immediately
                return;
            }
        }
    } else {
        // error may go below zero
        let mut error = delta_x_step - delta_y;

        while y1 != y2 {
            if error >= 0 {
                if error != 0 || (y_sign > 0) {
                    x1 += x_sign;
                    error -= delta_y_step;
                }
                // else do nothing
            }
            // else do nothing

            y1 += y_sign;
            error += delta_x_step;

            if callback(x1, y1) == false {
                return;
            }
        }
    }
}


/* ---------------------------------------------------------------------------
 * Draw Line */


fn draw_line(
    w: i32, h: i32,
    x0: i32, y0: i32,
    x1: i32, y1: i32)
{
    // Canvas
    let mut grid = vec![false; (w * h) as usize];

    // Plot onto the canvas
    {
        let mut callback = |x, y| {
            if x >= 0 && x < w &&
               y >= 0 && y < h
            {
                grid[(x + y * w) as usize] = true;
            }
            true
        };

        plot_line_v2v2i(
            [x0, y0],
            [x1, y1],
            &mut callback,
        );
    }

    // Draw the line as ASCII art
    {
        print!("|");
        for _ in 0..w {
            print!("-");
        }
        println!("|");

        for y in 0..h {
            print!("|");
            for x in 0..w {
                if grid[(x + y * w) as usize] {
                    print!("#", );
                } else {
                    print!(" ", );
                }
            }
            println!("|");
        }

        print!("|");
        for _ in 0..w {
            print!("-");
        }
        println!("|");
    }
}


/* ---------------------------------------------------------------------------
 * Parse Args */

/**
 * Extract args from a string:
 *
 *   "--foo=bar"
 *
 * Into:
 *
 *   ("foo", "bar")
 *
 */
fn param_extract_key_val<'a>(arg: &'a str) -> Option<(&'a str, &'a str)> {
    if arg.starts_with("--") {
        if let Some(i) = arg.find("=") {
            return Some((&arg[2..i], &arg[i + 1..]));
        }
    }
    return None;
}

struct ArgParams {
    width: Option<i32>,
    height: Option<i32>,
    pt_src_x0: Option<i32>,
    pt_src_y0: Option<i32>,
    pt_src_x1: Option<i32>,
    pt_src_y1: Option<i32>,
}

fn params_extract(p: &mut ArgParams) -> bool {
    for arg in std::env::args().skip(1) {
        if let Some((key, val)) = param_extract_key_val(&arg) {

            macro_rules! match_parse {
                ($out_val:expr, $T:ident) => {
                    /* ideally we could use typeof(out_val) */
                    $out_val = Some($T::from_str(&val).expect("failed to parse number"));
                }
            }

            match key {
                "x0" => match_parse!(p.pt_src_x0, i32),
                "y0" => match_parse!(p.pt_src_y0, i32),
                "x1" => match_parse!(p.pt_src_x1, i32),
                "y1" => match_parse!(p.pt_src_y1, i32),
                "width" => match_parse!(p.width, i32),
                "height" => match_parse!(p.height, i32),
                _ => {
                    println!("Unknown argument: {}", key);
                    std::process::exit(1);
                },
            }
        } else {
            println!("Unknown argument formatting: {}", arg);
            std::process::exit(1);
        }
    }

    return true;
}

fn main() {
    let mut p = ArgParams {
        width: None,
        height: None,
        pt_src_x0: None,
        pt_src_y0: None,
        pt_src_x1: None,
        pt_src_y1: None,
    };

    if !params_extract(&mut p) {
        // error already printed
        std::process::exit(1);
    }

    if elem!(None,
             p.width, p.height,
             p.pt_src_x0, p.pt_src_y0,
             p.pt_src_x1, p.pt_src_y1)
    {
        println!("Missing --x0/y0/x1/y1/width/height arguments");
        std::process::exit(1);
    }

    draw_line(
        p.width.unwrap(), p.height.unwrap(),
        p.pt_src_x0.unwrap(), p.pt_src_y0.unwrap(),
        p.pt_src_x1.unwrap(), p.pt_src_y1.unwrap());
    
}

