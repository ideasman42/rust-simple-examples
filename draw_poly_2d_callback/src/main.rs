/*
 * This is a simple, self contained example of how to fill a polygon on a grid (raster image),
 * using the following command:
 *
 * ./draw_poly_callback --width=80 --height=80 --coords=0,0/40,80/80,0/0,50/80,50
 *
 * This example is an example of:
 *
 * - Using a callback.
 * - Vector of tuples.
 * - Vector manipulation.
 * - Simple argument parsing.
 */

use std::str::FromStr;

/* ---------------------------------------------------------------------------
 * Utility Macro's */

macro_rules! elem {
    ($val:expr, $($var:expr), *) => {
        $($val == $var) || *
    }
}


/* ---------------------------------------------------------------------------
 * Plot Line */

/**
 *
 * - callback: Takes the x, y coords and x-span (x_end is not inclusive),
 *   note that `x_end` will always be greater than `x`.
 *
 */
fn fill_poly_v2i_n<F: FnMut(i32, i32, i32)>(
    xmin: i32, ymin: i32,
    xmax: i32, ymax: i32,
    coords: &Vec<[i32; 2]>,
    callback: &mut F)
{
    /* Originally by Darel Rex Finley, 2007.
     * Optimized by Campbell Barton, 2016 to keep sorted intersections. */

    /*
     * Note: all the index lookups here could be made unsafe
     * (as in, we know they won't fail).
     */

    // only because we use this with int values frequently, avoids casting every time.
    let coords_len: i32 = coords.len() as i32;

    let mut span_y: Vec<[i32; 2]> = Vec::with_capacity(coords.len());

    {
        let mut i_prev: i32 = coords_len - 1;
        let mut i_curr: i32 = 0;
        let mut co_prev = &coords[i_prev as usize];
        for co_curr in coords {
            if co_prev[1] != co_curr[1] {
                // Any segments entirely above or below the area of interest can be skipped.
                if (std::cmp::min(co_prev[1], co_curr[1]) >= ymax) ||
                   (std::cmp::max(co_prev[1], co_curr[1]) <  ymin)
                {
                    continue;
                }

                span_y.push(
                    if co_prev[1] < co_curr[1] {
                        [i_prev, i_curr]
                    } else {
                        [i_curr, i_prev]
                    });
            }
            i_prev = i_curr;
            i_curr += 1;
            co_prev = co_curr;
        }
    }

    // sort edge-segments on y, then x axis
    span_y.sort_by(
        |a, b| {
            let co_a = &coords[a[0] as usize];
            let co_b = &coords[b[0] as usize];
            let mut ord = co_a[1].cmp(&co_b[1]);
            if ord == std::cmp::Ordering::Equal {
                ord = co_a[0].cmp(&co_b[0]);
            }
            if ord == std::cmp::Ordering::Equal {
                // co_a & co_b are identical, use the line closest to the x-min
                let co = co_a; // could be co_b too.
                let co_a = &coords[a[1] as usize];
                let co_b = &coords[b[1] as usize];
                ord = 0.cmp(&(((co_b[0] - co[0]) * (co_a[1] - co[1])) -
                              ((co_a[0] - co[0]) * (co_b[1] - co[1]))));
            }
            ord
        }
    );

    // Used to store x intersections for the current y axis ('pixel_y')
    struct NodeX {
        span_y_index: usize,
        // 'x' pixel value for the current 'pixel_y'.
        x: i32,
    }
    let mut node_x: Vec<NodeX> = Vec::with_capacity(coords.len() + 1);
    let mut span_y_index: usize = 0;

    if span_y.len() != 0 && coords[span_y[0][0] as usize][1] < ymin {
        while (span_y_index < span_y.len()) &&
              (coords[span_y[span_y_index][0] as usize][1] < ymin)
        {
            assert!(coords[span_y[span_y_index][0] as usize][1] <
                    coords[span_y[span_y_index][1] as usize][1]);
            if coords[span_y[span_y_index][1] as usize][1] >= ymin {
                node_x.push(NodeX{ span_y_index: span_y_index, x: -1 });
            }
            span_y_index += 1;
        }
    }

    // Loop through the rows of the image.
    for pixel_y in ymin..ymax {

        let mut is_sorted = true;
        let mut do_remove = false;
        {
            let mut x_ix_prev = i32::min_value();
            for n in &mut node_x {
                let s = &span_y[n.span_y_index];
                let co_prev = &coords[s[0] as usize];
                let co_curr = &coords[s[1] as usize];

                assert!(co_prev[1] < pixel_y && co_curr[1] >= pixel_y);
                let x    = (co_prev[0] - co_curr[0]) as f64;
                let y    = (co_prev[1] - co_curr[1]) as f64;
                let y_px = (pixel_y    - co_curr[1]) as f64;
                let x_ix = ((co_curr[0] as f64) + ((y_px / y) * x)).round() as i32;
                n.x = x_ix;

                if is_sorted && (x_ix_prev > x_ix) {
                    is_sorted = false;
                }
                if do_remove == false && co_curr[1] == pixel_y {
                    do_remove = true;
                }
                x_ix_prev = x_ix;
            }
        }
        // Theres no reason this will ever be larger
        assert!(node_x.len() <= coords.len() + 1);

        // Sort the nodes, via a simple "bubble" sort.
        if is_sorted == false {
            let node_x_end = node_x.len() - 1;
            let mut i: usize = 0;
            while i < node_x_end {
                if node_x[i].x > node_x[i + 1].x {
                    node_x.swap(i, i + 1);
                    if i != 0 {
                        i -= 1;
                    }
                } else {
                    i += 1;
                }
            }
        }

        // Fill the pixels between node pairs.
        {
            // TODO, use `node_x.step_by(2)`. When its in stable
            let mut i = 0;
            while i < node_x.len() {
                let mut x_src = node_x[i].x;
                let mut x_dst = node_x[i + 1].x;

                if x_src >= xmax {
                    break;
                }

                if x_dst > xmin {
                    if x_src < xmin {
                        x_src = xmin;
                    }
                    if x_dst > xmax {
                        x_dst = xmax;
                    }

                    // Single call per x-span.
                    if x_src < x_dst {
                        callback(x_src - xmin, x_dst - xmin, pixel_y - ymin);
                    }
                }
                i += 2;
            }
        }

        // Clear finalized nodes in one pass, only when needed
        // (avoids excessive array-resizing).
        if do_remove {
            let mut i_dst: usize = 0;
            for i_src in 0..node_x.len() {
                let s = &span_y[node_x[i_src].span_y_index];
                let co = &coords[s[1] as usize];
                if co[1] != pixel_y {
                    if i_dst != i_src {
                        // x is initialized for the next pixel_y (no need to adjust here)
                        node_x[i_dst].span_y_index = node_x[i_src].span_y_index;
                    }
                    i_dst += 1;
                }
            }
            node_x.truncate(i_dst);
        }

        // Scan for new events
        {
            while span_y_index < span_y.len() &&
                  coords[span_y[span_y_index][0] as usize][1] == pixel_y
            {
                // Note, node_x these are just added at the end,
                // not ideal but sorting once will resolve.

                // x is initialized for the next pixel_y
                node_x.push(NodeX {span_y_index: span_y_index, x: -1});
                span_y_index += 1;
            }
        }
    }
}


/* ---------------------------------------------------------------------------
 * Draw Poly */


fn draw_poly(
    w: i32, h: i32,
    coords: &Vec<[i32; 2]>)
{
    // Canvas
    let mut grid = vec![false; (w * h) as usize];

    // Plot onto the canvas
    {
        let mut callback = |x_start: i32, x_end: i32, y: i32| {
            for x in x_start..x_end {
                if x >= 0 && x < w &&
                   y >= 0 && y < h
                {
                    grid[(x + y * w) as usize] = true;
                }
            }
            return;
        };

        fill_poly_v2i_n(
            0, 0, w, h,
            coords,
            &mut callback,
        );
    }

    // Draw the poly as ASCII art
    {
        print!("|");
        for _ in 0..w {
            print!("-");
        }
        println!("|");

        for y in 0..h {
            print!("|");
            for x in 0..w {
                if grid[(x + y * w) as usize] {
                    print!("#", );
                } else {
                    print!(" ", );
                }
            }
            println!("|");
        }

        print!("|");
        for _ in 0..w {
            print!("-");
        }
        println!("|");
    }
}


/* ---------------------------------------------------------------------------
 * Parse Args */

/**
 * Extract args from a string:
 *
 *   "--foo=bar"
 *
 * Into:
 *
 *   ("foo", "bar")
 *
 */
fn param_extract_key_val<'a>(arg: &'a str) -> Option<(&'a str, &'a str)> {
    if arg.starts_with("--") {
        if let Some(i) = arg.find("=") {
            return Some((&arg[2..i], &arg[i + 1..]));
        }
    }
    return None;
}

struct ArgParams {
    width: Option<i32>,
    height: Option<i32>,
    coords: Vec<[i32; 2]>,
}

fn params_extract(p: &mut ArgParams) -> bool {
    for arg in std::env::args().skip(1) {
        if let Some((key, val)) = param_extract_key_val(&arg) {

            macro_rules! match_parse {
                ($out_val:expr, $T:ident) => {
                    /* ideally we could use typeof(out_val) */
                    $out_val = Some($T::from_str(&val).expect("failed to parse number"));
                }
            }

            match key {
                "coords" => {
                    for co in val.split('/') {
                        let co_split = co.split(',').collect::<Vec<&str>>();
                        if co_split.len() != 2 {
                            println!("error expected {} to be 2 numbers", co);
                            std::process::exit(1);
                        } else {
                            p.coords.push([
                                i32::from_str(co_split[0]).expect("failed to parse number"),
                                i32::from_str(co_split[1]).expect("failed to parse number"),
                            ]);
                        }
                    }
                },
                "width" => match_parse!(p.width, i32),
                "height" => match_parse!(p.height, i32),
                _ => {
                    println!("Unknown argument: {}", key);
                    std::process::exit(1);
                },
            }
        } else {
            println!("Unknown argument formatting: {}", arg);
            std::process::exit(1);
        }
    }

    return true;
}

fn main() {
    let mut p = ArgParams {
        width: None,
        height: None,
        coords: vec![],
    };

    if !params_extract(&mut p) {
        // error already printed
        std::process::exit(1);
    }

    if elem!(None, p.width, p.height) {
        println!("Missing --width/height arguments");
        std::process::exit(1);
    }

    if p.coords.len() == 0 {
        println!("Missing --coords arguments or no coords passed");
        std::process::exit(1);
    }

    draw_poly(
        p.width.unwrap(), p.height.unwrap(),
        &p.coords);
}

#[cfg(test)]
#[path="tests.rs"] mod test;

