/*
 * This is a simple, self contained example of how to fill a polygon on a grid (raster image),
 * using the following command:
 *
 * ./draw_poly_callback --width=80 --height=80 --coords=0,0/40,80/80,0/0,50/80,50
 *
 * This example is an example of:
 *
 * - Using an iterator.
 *   Note that this probably isn't ideal use of an iterator,
 *   poly-filling in an iterator ended up being a little involved,
 *   the callback version is easier to follow.
 * - Vector of tuples.
 * - Vector manipulation.
 * - Simple argument parsing.
 */

use std::str::FromStr;

/* ---------------------------------------------------------------------------
 * Utility Macro's */

macro_rules! elem {
    ($val:expr, $($var:expr), *) => {
        $($val == $var) || *
    }
}


/* ---------------------------------------------------------------------------
 * Fill Poly */

/**
 *
 * - callback: Takes the x, y coords and x-span (x_end is not inclusive),
 *   note that `x_end` will always be greater than `x`.
 *
 */

struct FillPoly2D<'a> {
    // Constants
    xmin: i32,
    ymin: i32,
    xmax: i32,
    ymax: i32,
    coords: &'a Vec<[i32; 2]>,

    // Current y intersections.
    node_x: Vec<i32>,
    // Current node_x index
    node_x_index: usize,
    // Current y pixel
    pixel_y: i32,
}


impl <'a>Iterator for FillPoly2D<'a> {
    type Item = (i32, i32, i32);

    /* originally by Darel Rex Finley, 2007 */
    /*
     * note: all the index lookups here could be made unsafe
     * (as in, we know they won't fail).
     */

    fn next(&mut self) -> Option<(i32, i32, i32)> {
        // Step over each y-axis
        while self.pixel_y != self.ymax {

            // Check if we're in the middle of iterating over x-span's
            // if this is zero, its a trigger to build the next x-spans.
            if self.node_x.len() == 0 {
                // Loop through the rows of the image.
                let mut co_prev = self.coords.last().unwrap();
                assert!(self.node_x.len() == 0);
                for co_curr in self.coords {

                    if (co_curr[1] < self.pixel_y && co_prev[1] >= self.pixel_y) ||
                       (co_prev[1] < self.pixel_y && co_curr[1] >= self.pixel_y)
                    {
                        let x    = (co_prev[0] - co_curr[0]) as f64;
                        let y    = (co_prev[1] - co_curr[1]) as f64;
                        let y_px = (self.pixel_y    - co_curr[1]) as f64;
                        self.node_x.push((((co_curr[0] as f64) + ((y_px / y) * x)).round() as i32));
                    }
                    co_prev = co_curr;
                }
                // theres no reason this will ever be larger
                assert!(self.node_x.len() <= self.coords.len() + 1);


                // sort the nodes, via a simple "bubble" sort.
                if self.node_x.len() != 0 {
                    let mut i: usize = 0;
                    while i < self.node_x.len() - 1 {
                        if self.node_x[i] > self.node_x[i + 1] {
                            self.node_x.swap(i, i + 1);
                            if i != 0 {
                                i -= 1;
                            }
                        } else {
                            i += 1;
                        }
                    }
                }

                self.node_x_index = 0;
            }

            // Fill the pixels between node pairs.
            while self.node_x_index < self.node_x.len() {
                let mut x_src = self.node_x[self.node_x_index];
                if x_src >= self.xmax {
                    break;
                }
                let mut x_dst = self.node_x[self.node_x_index + 1];

                // Step to the next node_x no matter what happens next.
                // We have x_src & x_dst, don't access this again!
                self.node_x_index += 2;

                if x_dst >  self.xmin {
                    if x_src < self.xmin {
                        x_src = self.xmin;
                    }
                    if x_dst > self.xmax {
                        x_dst = self.xmax;
                    }

                    // for single call per x-span
                    if x_src < x_dst {
                        return Some((x_src - self.xmin, x_dst - self.xmin, self.pixel_y - self.ymin));
                    }
                }
            }

            // if nothing returns, move to the next y step.
            self.node_x.clear();
            self.pixel_y += 1;
        }
        return None;
    }
}

fn fill_poly_v2i_n_iter(
    xmin: i32, ymin: i32,
    xmax: i32, ymax: i32,
    coords: &Vec<[i32; 2]>
    ) -> FillPoly2D
{
    let node_x = Vec::with_capacity(coords.len() + 1);

    FillPoly2D {
        xmin: xmin,
        ymin: ymin,

        xmax: xmax,
        ymax: ymax,

        coords: coords,
        node_x: node_x,

        pixel_y: ymin,
        node_x_index: 0,
    }
}


/* ---------------------------------------------------------------------------
 * Draw Poly */

fn draw_poly(
    w: i32, h: i32,
    coords: &Vec<[i32; 2]>)
{
    // Canvas
    let mut grid = vec![false; (w * h) as usize];

    // Plot onto the canvas
    for (x_start, x_end, y) in fill_poly_v2i_n_iter(0, 0, w, h, coords) {
        for x in x_start..x_end {
            if x >= 0 && x < w &&
               y >= 0 && y < h
            {
                grid[(x + y * w) as usize] = true;
            }
        }
    }

    // Draw the poly as ASCII art
    {
        print!("|");
        for _ in 0..w {
            print!("-");
        }
        println!("|");

        for y in 0..h {
            print!("|");
            for x in 0..w {
                if grid[(x + y * w) as usize] {
                    print!("#", );
                } else {
                    print!(" ", );
                }
            }
            println!("|");
        }

        print!("|");
        for _ in 0..w {
            print!("-");
        }
        println!("|");
    }
}


/* ---------------------------------------------------------------------------
 * Parse Args */

/**
 * Extract args from a string:
 *
 *   "--foo=bar"
 *
 * Into:
 *
 *   ("foo", "bar")
 *
 */
fn param_extract_key_val<'a>(arg: &'a str) -> Option<(&'a str, &'a str)> {
    if arg.starts_with("--") {
        if let Some(i) = arg.find("=") {
            return Some((&arg[2..i], &arg[i + 1..]));
        }
    }
    return None;
}

struct ArgParams {
    width: Option<i32>,
    height: Option<i32>,
    coords: Vec<[i32; 2]>,
}

fn params_extract(p: &mut ArgParams) -> bool {
    for arg in std::env::args().skip(1) {
        if let Some((key, val)) = param_extract_key_val(&arg) {

            macro_rules! match_parse {
                ($out_val:expr, $T:ident) => {
                    /* ideally we could use typeof(out_val) */
                    $out_val = Some($T::from_str(&val).expect("failed to parse number"));
                }
            }

            match key {
                "coords" => {
                    for co in val.split('/') {
                        let co_split = co.split(',').collect::<Vec<&str>>();
                        if co_split.len() != 2 {
                            println!("error expected {} to be 2 numbers", co);
                            std::process::exit(1);
                        } else {
                            p.coords.push([
                                i32::from_str(co_split[0]).expect("failed to parse number"),
                                i32::from_str(co_split[1]).expect("failed to parse number"),
                            ]);
                        }
                    }
                },
                "width" => match_parse!(p.width, i32),
                "height" => match_parse!(p.height, i32),
                _ => {
                    println!("Unknown argument: {}", key);
                    std::process::exit(1);
                },
            }
        } else {
            println!("Unknown argument formatting: {}", arg);
            std::process::exit(1);
        }
    }

    return true;
}

fn main() {
    let mut p = ArgParams {
        width: None,
        height: None,
        coords: vec![],
    };

    if !params_extract(&mut p) {
        // error already printed
        std::process::exit(1);
    }

    if elem!(None, p.width, p.height) {
        println!("Missing --width/height arguments");
        std::process::exit(1);
    }

    if p.coords.len() == 0 {
        println!("Missing --coords arguments or no coords passed");
        std::process::exit(1);
    }

    draw_poly(
        p.width.unwrap(), p.height.unwrap(),
        &p.coords);
}

