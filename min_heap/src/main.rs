mod min_heap;

fn main() {

    use min_heap;

    {
        let mut h = min_heap::MinHeap::new();
        h.insert(3.0, 0);
        h.insert(2.0, 1);
        h.insert(1.0, 2);
        h.insert(0.0, 3);

        assert!(h.pop_min().unwrap() == 3);
        assert!(h.pop_min().unwrap() == 2);
        assert!(h.pop_min().unwrap() == 1);
        assert!(h.pop_min().unwrap() == 0);
    }

    {
        let mut h = min_heap::MinHeap::with_capacity(4);
        h.insert(-3.0, "Hello");
        let b = h.insert(-2.0, "World");
        let c = h.insert(-1.0, "Test");
        h.insert(-0.0, "123");

        assert!(b != c);

        h.remove(b);
        h.remove(c);

        assert!(h.pop_min().unwrap() == "Hello");
        assert!(h.pop_min().unwrap() == "123");
    }
}

