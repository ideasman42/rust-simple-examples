// based on
// https://developer.blender.org/diffusion/B/browse/master/source/blender/blenlib/intern/BLI_kdtree.c

const KD_NODE_UNSET: u32 = std::u32::MAX;
const KD_AXIS_DIMS: usize = 3;

macro_rules! unlikely { ($body:expr) => { $body } }

struct KDTreeNode {
    neg: u32,
    pos: u32,
    co: [f64; KD_AXIS_DIMS],
    // typically an index but can be used for any id
    id: i32,

    axis: u32,  /* range is only (0-2) for 3d kdtree's */
}

struct KDTree {
    nodes: Vec<KDTreeNode>,
    root: u32,

    // ensure we call balance first
    is_balanced: bool,
    // max size of the tree
    maxsize: usize,
}

struct KDTreeNearest {
    id: i32,
    dist_sq: f64,
    co: [f64; KD_AXIS_DIMS],
}


// Construction: first insert points, then call balance before doing lookups.
impl KDTree {

    fn len_squared_vnvn(
        a: &[f64; KD_AXIS_DIMS],
        b: &[f64; KD_AXIS_DIMS]
    ) -> f64
    {
        let mut f = 0.0;
        for i in 0..KD_AXIS_DIMS {
            unsafe {
                f += (a.get_unchecked(i) - b.get_unchecked(i)).powi(2);
            }
        }
        return f;
    }

    pub fn new(maxsize: usize) -> KDTree {
        let nodes = Vec::with_capacity(maxsize);

        KDTree {
            nodes: nodes,
            root: KD_NODE_UNSET,

            is_balanced: false,
            maxsize: maxsize,
        }
    }

    pub fn insert(self: &mut KDTree, id: i32, co: &[f64; KD_AXIS_DIMS]) {
        assert!(self.nodes.len() <= self.maxsize);
        self.nodes.push(
            KDTreeNode {
                neg: KD_NODE_UNSET,
                pos: KD_NODE_UNSET,
                co: *co,
                id: id,
                // dummy value
                axis: 0,
            }
        );

        // could be debug-only
        self.is_balanced = false;
    }
    pub fn balance(self: &mut KDTree) {

        fn balance_recursive(
            nodes: &mut [KDTreeNode],
            axis: usize, ofs: u32) -> u32
        {
            if nodes.len() <= 0 {
                return KD_NODE_UNSET;
            } else if nodes.len() == 1 {
                return 0 + ofs;
            }

            // quick-sort style sorting around median
            let median = nodes.len() / 2;
            {
                let mut neg = 0;
                let mut pos = nodes.len() - 1;

                // note that we could avoid swapping the 'd' for some minor performance improvement.
                while pos > neg {
                    let co = unsafe { nodes.get_unchecked(pos).co[axis] };
                    let mut i = neg.wrapping_sub(1);
                    let mut j = pos;

                    loop {
                        unsafe {
                            while nodes.get_unchecked({i = i.wrapping_add(1); i}).co[axis] < co {}
                            while nodes.get_unchecked({j = j.wrapping_sub(1); j}).co[axis] > co && j > neg {}
                        }

                        if i >= j {
                            break;
                        }

                        nodes.swap(i, j);
                    }

                    nodes.swap(i, pos);
                    if i >= median {
                        pos = i - 1;
                    }
                    if i <= median {
                        neg = i + 1;
                    }
                }
            }

            // set node and sort subnodes
            let axis_next = (axis + 1) % KD_AXIS_DIMS;
            let node_neg = balance_recursive(&mut nodes[..median], axis_next, ofs);
            let node_pos = balance_recursive(&mut nodes[(median + 1)..], axis_next, (median as u32 + 1) + ofs);
            {
                let mut node = unsafe { nodes.get_unchecked_mut(median) };
                node.axis = axis as u32;
                node.neg = node_neg;
                node.pos = node_pos;
            }

            return median as u32 + ofs;
        }

        self.root = balance_recursive(&mut self.nodes[..], 0, 0);
        self.is_balanced = true;
    }


    fn _find_nearest_node(self: &KDTree, co: &[f64; KD_AXIS_DIMS]) -> (&KDTreeNode, f64)
    {
        // local structs to pass to recursive function
        struct FindData<'a> {
            nodes: &'a Vec<KDTreeNode>,
            co: &'a [f64; KD_AXIS_DIMS],
        }

        struct FindResult {
            min_dist_sqr: f64,
            min_index: u32,
        }

        fn _find_nearest_node_recurse(
            data: &FindData,
            node_index: u32,
            found: &mut FindResult)
        {

            let node = unsafe { &data.nodes.get_unchecked(node_index as usize) };
            let mut cur_dist = node.co[node.axis as usize] - data.co[node.axis as usize];
            if cur_dist < 0.0 {
                cur_dist = -cur_dist * cur_dist;

                if -cur_dist < found.min_dist_sqr {
                    cur_dist = KDTree::len_squared_vnvn(&node.co, data.co);
                    if cur_dist < found.min_dist_sqr {
                        found.min_dist_sqr = cur_dist;
                        found.min_index = node_index;
                    }
                    if node.pos != KD_NODE_UNSET {
                        _find_nearest_node_recurse(data, node.pos, found);
                    }
                    if node.neg != KD_NODE_UNSET {
                        _find_nearest_node_recurse(data, node.neg, found);
                    }
                } else {
                    if node.pos != KD_NODE_UNSET {
                        _find_nearest_node_recurse(data, node.pos, found);
                    }
                }
            } else {
                cur_dist = cur_dist * cur_dist;

                if cur_dist < found.min_dist_sqr {
                    cur_dist = KDTree::len_squared_vnvn(&node.co, data.co);
                    if cur_dist < found.min_dist_sqr {
                        found.min_dist_sqr = cur_dist;
                        found.min_index = node_index;
                    }
                    if node.neg != KD_NODE_UNSET {
                        _find_nearest_node_recurse(data, node.neg, found);
                    }
                    if node.pos != KD_NODE_UNSET {
                        _find_nearest_node_recurse(data, node.pos, found);
                    }
                } else {
                    if node.neg != KD_NODE_UNSET {
                        _find_nearest_node_recurse(data, node.neg, found);
                    }
                }
            }
        }


        let nodes = &self.nodes;

        assert!(self.root != KD_NODE_UNSET);

        let root = unsafe { &nodes.get_unchecked(self.root as usize) };

        let recurse_data = FindData {
            nodes: nodes,
            co: co,
        };

        let mut found = FindResult {
            min_index: self.root,
            min_dist_sqr: KDTree::len_squared_vnvn(&root.co, co),
        };

        if co[root.axis as usize] > root.co[root.axis as usize] {
            if unlikely!(root.pos != KD_NODE_UNSET) {
                _find_nearest_node_recurse(&recurse_data, root.pos, &mut found);
            }
            if unlikely!(root.neg != KD_NODE_UNSET) {
                _find_nearest_node_recurse(&recurse_data, root.neg, &mut found);
            }
        } else {
            if unlikely!(root.neg != KD_NODE_UNSET) {
                _find_nearest_node_recurse(&recurse_data, root.neg, &mut found);
            }
            if unlikely!(root.pos != KD_NODE_UNSET) {
                _find_nearest_node_recurse(&recurse_data, root.pos, &mut found);
            }
        }


        return (unsafe { &nodes.get_unchecked(found.min_index as usize) }, found.min_dist_sqr);
    }

    pub fn find_nearest(
            self: &KDTree, co: &[f64; KD_AXIS_DIMS]) -> KDTreeNearest
    {
        assert!(self.is_balanced == true);
        assert!(self.root != KD_NODE_UNSET);

        let (min_node, min_dist) = self._find_nearest_node(co);

        return KDTreeNearest {
            id: min_node.id,
            dist_sq: min_dist,
            co: min_node.co,
        };
    }

    pub fn find_nearest_id(
            self: &KDTree, co: &[f64; KD_AXIS_DIMS]) -> i32
    {
        assert!(self.is_balanced == true);
        assert!(self.root != KD_NODE_UNSET);

        let (min_node, _) = self._find_nearest_node(co);

        return min_node.id;
    }
}


fn main() {
    let mut tree = KDTree::new(4);
    tree.insert(0, &[1.0, 2.0, 3.0]);
    tree.insert(1, &[2.0, 3.0, 1.0]);
    tree.insert(2, &[3.0, 1.0, 2.0]);
    tree.insert(3, &[3.0, 2.0, 1.0]);

    tree.balance();

    let near = tree.find_nearest(&[3.0, 1.0, 2.0]);

    println!("found {} {} {:?}!", near.id, near.dist_sq, near.co);

    let near = tree.find_nearest_id(&[3.0, 1.0, 2.0]);
    println!("found {}!", near);
    // println!("");
}


#[test]
fn kdtree_test_100() {
    let mut coords = Vec::with_capacity(100);

    let mut rand = vec![1.0; KD_AXIS_DIMS];

    for i in 0..100 {
        rand[0] = rand[0] + ((rand[1] - rand[2]) / 3.0 - i as f64);
        rand[1] = rand[1] + ((rand[2] - rand[0]) / 5.0 - i as f64);
        rand[2] = rand[2] + ((rand[0] - rand[1]) / 7.0 - i as f64);

        coords.push([rand[0], rand[1], rand[2]]);

        // println!("found {} {} {}!", rand[0], rand[1], rand[2]);
    }

    let mut tree = KDTree::new(100);
    for (i, v) in coords.iter().enumerate() {
        tree.insert(i as i32, &v);
    }
    tree.balance();

    for (i, v) in coords.iter().enumerate() {
        let near = tree.find_nearest(v);
        assert!(i as i32 == near.id);
        assert!(near.dist_sq == 0.0);
        assert!(*v == near.co);
    }
}

